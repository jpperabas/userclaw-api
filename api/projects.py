from app import db
from api import bp
from flask import jsonify, request, url_for
from datetime import datetime
from models.project import Project
from api.errors import bad_request

@bp.route('/projects/<int:id>', methods=['GET'])
def get_project(id):
    include_key = request.args.get('include_key', False, type=bool)
    return jsonify(Project.query.get_or_404(id).to_dict(include_key=include_key))

@bp.route('/projects', methods=['GET'])
def get_projects():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Project.to_collection_dict(Project.query, page, per_page, 'api.get_projects')
    return jsonify(data)

@bp.route('/projects', methods=['POST'])
def create_project():
    data = request.get_json() or {}
    if 'name' not in data or 'base_url' not in data:
        return bad_request('Please include name and base_url fields.')
    if Project.query.filter_by(base_url=data['base_url']).first():
        return bad_request('This base_url already has a related project.')
    project = Project()
    data['user_id'] = 1
    project.from_dict(data)
    db.session.add(project)
    db.session.commit()
    response = jsonify(project.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_project', id=project.id)
    return response
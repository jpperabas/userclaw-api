from app import db
from api import bp
from flask import jsonify, request, url_for
from datetime import datetime
from models.user import User
from api.errors import bad_request

@bp.route('/users/<int:id>', methods=['GET'])
def get_user(id):
    include_email = request.args.get('include_email', False, type=bool)
    return jsonify(User.query.get_or_404(id).to_dict(include_email=include_email))

@bp.route('/users', methods=['GET'])
def get_users():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(User.query, page, per_page, 'api.get_users')
    return jsonify(data)

@bp.route('/users', methods=['POST'])
def create_user():
    data = request.get_json() or {}
    if 'username' not in data or 'email' not in data or 'password' not in data:
        return bad_request('Please include username, email and password fields.')
    if User.query.filter_by(username=data['username']).first():
        return bad_request('This username has already been used.')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('An account with this email already exists.')
    user = User()
    user.from_dict(data, new_user=True)
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_user', id=user.id)
    return response

@bp.route('/users/<int:id>', methods=['PATCH'])
def update_user(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    if 'username' in data and data['username'] != user.username and \
            User.query.filter_by(username=data['username']).first():
        return bad_request('This username is allready asigned to an other user. Please use a different one.')
    if 'email' in data and data['email'] != user.email and \
            User.query.filter_by(email=data['email']).first():
        return bad_request('This username is allready asigned to an other user. Please use a different one.')
    user.from_dict(data, new_user=False)
    user.modified_at = datetime.utcnow()
    db.session.commit()
    return jsonify(user.to_dict())
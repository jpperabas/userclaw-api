from flask import Flask
from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt

import config

app = Flask(__name__)
api = Api(app)
app.config.from_object(config.DevelopmentConfig)
db = SQLAlchemy(app)
ma = Marshmallow(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)

from api import bp as api_bp
app.register_blueprint(api_bp, url_prefix='/api')
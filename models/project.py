from flask import url_for
from app import db, bcrypt
from datetime import datetime

from helpers.paginator import Paginator
from helpers.key_generator import KeyGenerator

class Project(Paginator, KeyGenerator, db.Model):
    __tablename__ = 'projects'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    key = db.Column(db.String(8), index=True, unique=True, nullable=False)
    base_url = db.Column(db.String(64), index=True, unique=True, nullable=False)
    name = db.Column(db.String(64), nullable=False)
    created_at = db.Column(db.DateTime, default = datetime.utcnow)
    modified_at = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    
    def __init__(self, name='', base_url=''):
        self.name = name
        self.base_url = base_url
        self.prefix = "p"
        self.key = self.random_key()
    
    def to_dict(self, include_key=False):
        data = {
            'id': self.id,
            'base_url': self.base_url,
            'name': self.name,
            'created_at': self.created_at,
            'modified_at': self.modified_at,
            '_links': {
                'self': url_for('api.get_project', id=self.id)
            }
        }
        if include_key:
            data['key'] = self.key
        return data

    def from_dict(self, data):
        for field in ['name', 'base_url']:
            if field in data:
                setattr(self, field, data[field])
    
    def __repr__(self):
        return '<Project {}>'.format(self.name)
from flask import url_for
from app import db, bcrypt
from datetime import datetime

from helpers.paginator import Paginator

class User(Paginator, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    email = db.Column(db.String(128), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    created_at = db.Column(db.DateTime, default = datetime.utcnow)
    modified_at = db.Column(db.DateTime)
    
    def set_password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def __init__(self, username='', email=''):
        self.username = username
        self.email = email
    
    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'created_at': self.created_at,
            'modified_at': self.modified_at,
            '_links': {
                'self': url_for('api.get_user', id=self.id)
            }
        }
        if include_email:
            data['email'] = self.email
        return data

    def from_dict(self, data, new_user=False):
        for field in ['username', 'email']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])
    
    def __repr__(self):
        return '<User {}>'.format(self.username)
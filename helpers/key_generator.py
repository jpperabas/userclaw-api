import random
import string

class KeyGenerator(object):
    def __init__(self, prefix):
        self.prefix = prefix
    def random_key(self):
        key = [self.prefix]
        for i in range(6):
            if random.randint(1,62) <= 10:
                random_digit = str(random.randint(0,9))
            else:
                random_digit = random.choice(string.ascii_letters)
            key.append(random_digit)
        return ''.join(key)